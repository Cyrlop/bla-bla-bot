# Blabla Bot
Playground bot to try speech to text and text to speech in Python

## Usage
```
usage: main.py [-h]

Blabla Bot for discord.

optional arguments:
  -h, --help         show this help message and exit
```

## Commands
Use `!bla ` to invoke the bot and use one of the following commands:
- `join`: Joins current voice channel
- `leave`: Leaves current voice channel
- `say <text>`: Joins current voice channel and say the text using text to speech (English)
- `dis <text>`: Joins current voice channel and say the text using text to speech (French)
