import uuid
from gtts import gTTS


def create_tts_file(message, lang="en"):
    # TODO: Use proper tempfile files
    file_path = f"/tmp/tts_{uuid.uuid1()}.mp3"
    tts = gTTS(message, lang=lang)

    with open(file_path, "wb") as f:
        tts.write_to_fp(f)

    return file_path
