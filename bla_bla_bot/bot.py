import os
import discord
from discord.ext import commands

from speech_utils import create_tts_file


def get_intents():
    intents = discord.Intents.default()
    intents.typing = False
    intents.presences = False
    intents.messages = True
    intents.guilds = True
    intents.reactions = True
    return intents


discord_bot_token = os.environ.get("DISCORD_BOT_TOKEN_BLABLA")
intents = get_intents()
command_prefix = "!bla "

bot = commands.Bot(
    command_prefix=commands.when_mentioned_or(command_prefix),
    description="Blabla Bot for discord",
    intents=intents,
)
bot.voice = None
bot.channel = None


@bot.event
async def on_ready():
    print(f"Discord client started and logged on as {bot.user}!")
    print("------")


@bot.command()
async def join(ctx):
    if bot.channel is not None and bot.channel != ctx.author.voice.channel:
        await leave(ctx)

    if bot.voice is None:
        bot.channel = ctx.author.voice.channel
        bot.voice = await bot.channel.connect()


@bot.command()
async def leave(ctx):
    try:
        await ctx.voice_client.disconnect()
    except:
        print("Couldn't disconnect")
    bot.channel = None
    bot.voice = None


@bot.command()
async def say(ctx, *, args=None, lang="en"):
    message = args
    tts_file_path = create_tts_file(message, lang=lang)

    await join(ctx)

    # Can use: after=lambda e: print("done", e)
    bot.voice.play(discord.FFmpegPCMAudio(tts_file_path))

    while bot.voice.is_playing():
        pass

    os.remove(tts_file_path)


@bot.command()
async def dis(ctx, *, args=None):
    await say(ctx, lang="fr", args=args)


bot.run(discord_bot_token)
